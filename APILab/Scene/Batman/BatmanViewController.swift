//
//  BatmanViewController.swift
//  APILab
//
//  Created by admin on 7/30/19.
//  Copyright © 2019 promptNow. All rights reserved.
//

import UIKit

class BatmanViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.image = UIImage.gifImageWithName("batmanGif")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickButton() {
        dismiss(animated: true, completion: nil)
    }
}
