//
//  VibrateViewController.swift
//  APILab
//
//  Created by admin on 8/26/19.
//  Copyright © 2019 promptNow. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class VibrateViewController: UIViewController {
    
    var vibrateFlag: Bool = false
    var alertSoundEffect: AVAudioPlayer?
    
    let notificationFeedbackGenerator = UINotificationFeedbackGenerator()

    let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //notificationFeedbackGenerator.prepare()
       // lightImpactFeedbackGenerator.prepare()
    }
    
    @IBAction func didClickBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shotVibrate() {
        UIDevice.vibrate()
//        UIDevice.vibrate(sec: 3)
    }
    
    @IBAction func withSound() {
        playSound()
    }
    
    func playSound() {
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSession.Category.playback)
        
        DispatchQueue.global().async
            {
                let path = Bundle.main.path(forResource: "sound1.mp3", ofType:nil)!
                let url = URL(fileURLWithPath: path)
                
                do {
                    self.alertSoundEffect = try AVAudioPlayer(contentsOf: url)
                    if let alertSoundEffect = self.alertSoundEffect {
                        alertSoundEffect.prepareToPlay()
                        alertSoundEffect.play()
                    } else {
                        print("ERROR alertSoundEffect")
                    }
                } catch {
                    print("ERROR DO")
                    return
                }
        }
    }
}

// For Vibrate
import AudioToolbox
extension UIDevice {
    static func vibrate() {
        let feedbackSupportLevel = UIDevice.current.value(forKey: "_feedbackSupportLevel") as? Int
        switch feedbackSupportLevel {
        case 1:
            AudioServicesPlaySystemSound(1519) // Actuate `Peek` feedback (weak boom)
            //AudioServicesPlaySystemSound(1520) // Actuate `Pop` feedback (strong boom)
            //AudioServicesPlaySystemSound(1521) // Actuate `Nope` feedback (series of three weak booms)
        case 2:
            let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .heavy)
            impactFeedbackgenerator.prepare()
            impactFeedbackgenerator.impactOccurred()
        default:
            print("NOT SUPPORT SHOT VIBRATE")
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
    static func vibrate(sec: Int) {
        for _ in 1...sec {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            sleep(1)
        }
    }
}
