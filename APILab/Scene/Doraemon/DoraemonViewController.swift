//
//  DoraemonViewController.swift
//  APILab
//
//  Created by admin on 7/30/19.
//  Copyright © 2019 promptNow. All rights reserved.
//

import UIKit

class DoraemonViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.image = UIImage.gifImageWithName("doraemonGif")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: imageView)
        }
    }
    
    @IBAction func didClickButton() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension DoraemonViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let destVC = storyboard.instantiateViewController(withIdentifier: "BATMAN")
        destVC.preferredContentSize = CGSize.init(width: (self.view.bounds.width*0.8), height: (self.view.bounds.height*0.8))
        navigationController?.pushViewController(destVC, animated: true)
        
        return destVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
    }
    
    
    
}
