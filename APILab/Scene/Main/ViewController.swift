//
//  ViewController.swift
//  APILab
//
//  Created by admin on 7/30/19.
//  Copyright © 2019 promptNow. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var counterLabel: UILabel!
    
    @IBOutlet weak var batmanButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapButton() {
        counterLabel.text = String((Int(counterLabel.text ?? "0") ?? 0) + 1)
    }

}
