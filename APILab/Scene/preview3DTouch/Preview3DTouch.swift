//
//  preview3DTouch.swift
//  APILab
//
//  Created by admin on 8/26/19.
//  Copyright © 2019 promptNow. All rights reserved.
//

/* STEP FOR INSTALL 3D TOUCH
 
 1. Check is function available and then regist forcetouch by example function name "configForForceTouch"
 2. setup "UIViewControllerPreviewingDelegate" to tell the app what going on when do force touch
 
 */


import UIKit
class Preview3DTouch: UIViewController {
    
    @IBOutlet weak var doraemonView: UIView!
    @IBOutlet weak var batmanView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configForForceTouch()
    }
    
    func configForForceTouch() {
        print("forceTouchCapability = > ", traitCollection.forceTouchCapability.rawValue)
        print(doraemonView.traitCollection.forceTouchCapability.rawValue)
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: doraemonView)
            registerForPreviewing(with: self, sourceView: batmanView)
        } else {
            print("REGIST FAIL!")
        }
    }
}


extension Preview3DTouch: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        /* SET THE ACTION WHENT PUSHING FORCE TOUCH */
        /* parameter
         - previewingContext is object location that on pushing
         - location is a location that pushing on screen but not stable for check (i dont know why)
        */
        
        print("FORCE TOUCH POSITION -> ", location)
        print("FORCE TOUCH OBJECT FRAME -> ", previewingContext.sourceRect)
        
        if batmanView.bounds.contains(previewingContext.sourceRect) {
            print("BATMAN TOUCH!!")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let destVC = storyboard.instantiateViewController(withIdentifier: "BATMAN")
            let screenSize = UIScreen.main.bounds
            destVC.preferredContentSize = CGSize.init(width: screenSize.width, height: screenSize.height)
            navigationController?.pushViewController(destVC, animated: true)
            return destVC
        }
        
        if doraemonView.bounds.contains(previewingContext.sourceRect) {
            print("DORAEMON TOUCH!!")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let destVC = storyboard.instantiateViewController(withIdentifier: "DORAEMON")
            let screenSize = UIScreen.main.bounds
            destVC.preferredContentSize = CGSize.init(width: screenSize.width, height: screenSize.height)
            navigationController?.pushViewController(destVC, animated: true)
            return destVC
        }
        
        return UIViewController()
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        /* SHOW PAGE AFTER FINISH FORCE TOUCH */
        /* let this empty if do nothing after finish force touch
         parameter
         - viewControllerToCommit is view controler that return from function previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint)
         - previewingContext is object that on finish touching
         */
        
        //        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        //        let destVC = storyboard.instantiateViewController(withIdentifier: "DetailVC")
        //        (destVC as? DetailVC)?.imgData = peekedData
        //        show(destVC, sender: self)
        
        show(viewControllerToCommit, sender: self)
        
    }
    
    
}
